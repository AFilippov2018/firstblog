<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Авторизация</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/signin.css" rel="stylesheet">
    <link href="/css/alertify.css" rel="stylesheet">
</head>

<body class="text-center">
<form class="form-signin" method="post">
    {!! csrf_field() !!}
    <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
    <h1 class="h3 mb-3 font-weight-normal">Авторизация</h1>
    <label for="inputEmail" class="sr-only">Email адрес</label>
    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email адрес" required autofocus>
    <label for="inputPassword" class="sr-only">Пароль</label>
    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Пароль" required>

    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" name="remember" value="1"> Запомнить меня
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Войти на сайт</button>
    <br>
    <br>

    <a href="/">На главную</a><br>
    <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
</form>
<script src="/js/alertify.js"></script>
@include('inc.messages')
</body>
</html>
