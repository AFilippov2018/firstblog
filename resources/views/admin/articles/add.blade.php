@extends('layouts.admin')
@section('content')
  <?php
    if (session()->has('errors')) {
        $errors = session()-> get('errors');
        foreach ($errors->all(":message") as $message) {
            echo $message . "<br>";
        }
    }
  ?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

        <h1 class="h2">Добавить статью</h1>
        <br>
        <form method="post">
            {!! csrf_field() !!}
            <p>Выбор категории:<br><select name="categories[]" class="form-control" multiple>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                </select></p>
            <p>Название статьи:<br><input type="text" name="title" class="form-control" required></p>
            <p>Автор:<br><input  name="author" class="form-control" required></p>
            <p>Короткий текст:<br><textarea name="short_text" class="form-control" required></textarea></p>
            <p>Полный текст:<br><textarea name="full_text" class="form-control"></textarea></p>
            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </main>
@stop