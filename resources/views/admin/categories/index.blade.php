@extends('layouts.admin')
@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

        <h1 class="h2">Категории</h1>
        <br>
        <a href="{{route('categories.add')}}" class="btn btn-info">Добавить категорию</a>
        <br>
        <br>
        <br>
        <table class="table-bordered">
            <tr>
                <th>#</th>
                <th>Наименование</th>
                <th>Описание</th>
                <th>Дата добавления</th>
                <th>Действия</th>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->title}}</td>
                    <td>{!!$category->description !!}</td>
                    <td>{{$category->created_at->format('d.m.Y H:i')}}</td>
                    <td><a href="{{ route('categories.edit', ['id' => $category->id])}}">Редактировать</a>||<a
                                href="{{route('categories.delete',['id'=>$category->id])}}"
                                onclick="return window.confirm('Вы уверены что хотите удалить эту категорию?')">Удалить</a>﻿
                    </td>
                </tr>
            @endforeach
        </table>
    </main>

